# BFI Article Thumbnail Viewer

Based on create-react-app purely for speed of initial set-up - in practice I would generally taylor build tools to only what's needed by a given application.

Use `npm install` to get set up, `npm start` to start the demo server, and `npm test` to run the tests.

## About

The app displays a grid of thumbnails for BFI articles. The thumbnails can be filered by article author and article type using the form controls on the page. Each tile scales up slightly on hover or focus - the original intention was to expand the tile to show the full article details, but unfortunately time got away from me.

### Accessibility

The grid is keyboard accessible, each tile having a tabIndex allowing keyboard users to tab through.

Given time I would have added server-side rendering to help users on poor connections.
Using a `form` element for data input is part of this process, the next step of which would be to
move the form submission handler to an abstracted layer.

## Performance improvements

The most glaring issue currently is the slow loading of images, since they use the full-size images.
Where smaller thumbnails aren't available via the API, the Node app can improve performance by resizing thumbnails either all up-front or the first time each image is requested.
