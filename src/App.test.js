import { render, screen } from '@testing-library/react';
import App from './App';

test('calls bfi client on render', () => {
  const mockBfiClient = {
    getArticles: jest.fn(()=>[]),
    getAuthors: jest.fn(()=>[]),
    getTypes: jest.fn(()=>[])
  }
  render(<App bfi={mockBfiClient} author='author-id'/>);

  expect(mockBfiClient.getArticles).toHaveBeenCalledWith({author: 'author-id'})
});
