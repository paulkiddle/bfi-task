
import './articles.css';

export default function Articles({ articles }) {
	return <div className="Articles">
		{articles.map(article=>
			<img
				key={article.primary_image.url}
				className="Article Article__thumb"
				src={article.primary_image.url}
				title={article.title}
				alt={article.primary_image.caption}
				tabIndex="0"></img>
		)}
	</div>
}
