import { render, screen } from '@testing-library/react';
import Articles from './articles.js';

test('renders articles', () => {
  const rendered = render(<Articles articles={[
		{
			primary_image: {
				url: 'http://example.com',
				caption: 'Example caption'
			},
			title: 'Article title'
		}
	]}/>);

  expect(rendered.container).toMatchSnapshot()
});
