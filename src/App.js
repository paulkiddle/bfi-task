import Articles from './articles/articles.js';
import React from 'react';

class App extends React.Component {
  #bfi

  constructor({ bfi, author, type }) {
    super();
    this.#bfi = bfi;
    this.state = {
      articles: [],
      authors: [],
      types: [],
      author,
      type
    }
  }

  async componentDidMount(){
    this.setState({
      articles: await this.#bfi.getArticles({ author: this.state.author, type: this.state.type }),
      authors: await this.#bfi.getAuthors(),
      types: await this.#bfi.getTypes()
    });
  }

  async submitForm(event) {
    event.preventDefault();
    const author = event.target.author.value;
    const type = event.target.type.value;

    this.setState({
      author,
      type,
      articles: await this.#bfi.getArticles({ author, type }),
    })
  }

  render(){
    return <div>
      <form
        style={{position: 'absolute', background: 'rgba(255, 255, 255, 0.7)', zIndex: 2}}
        onSubmit={this.submitForm.bind(this)}>
        <label>
          Author:
          <select name="author" defaultValue={this.state.author}>
            <option value="">All</option>
            {this.state.authors.map(author => <option value={author.id}>
              {author.name}
            </option>)}
          </select>
        </label>
        <label>
          Type:
          <select name="type" defaultValue={this.state.type}>
            <option value="">All</option>
            {this.state.types.map(type => <option value={type.id}>
              {type.name}
            </option>)}
          </select>
          <button>Filter</button>
        </label>
      </form>
      <Articles articles={this.state.articles}></Articles>
    </div>
  }
}

export default App;
