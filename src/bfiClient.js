export default class BfiClient {
	#base = 'https://content-store.explore.bfi.digital/api';

	async fetch(path, { author, type } = {}){
		const params = new URLSearchParams();
		if(author){
			params.set('author', author);
		}
		if(type){
			params.set('type', type);
		}
		const url = this.#base + path + (params ? '?' + params : '');

		const res = await fetch(url);

		return res.json();
	}

	async getArticles(params){
		const {data} = await this.fetch('/articles', params);
		return data;
	}

	async getAuthors(){
		const {data} = await this.fetch('/authors');
		return data;
	}

	async getTypes(){
		const {data} = await this.fetch('/types');
		return data;
	}
}
