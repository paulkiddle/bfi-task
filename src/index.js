import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import BfiClient from './bfiClient.js';

const bfiClient = new BfiClient();
const {searchParams} = new URL(window.location);

ReactDOM.render(
  <React.StrictMode>
    <App author={searchParams.get('author') || null} type={searchParams.get('type') || null} bfi={bfiClient} />
  </React.StrictMode>,
  document.getElementById('root')
);
